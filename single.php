<?php get_header(); ?>
<div class="wrapper section medium-padding" id="site-content">								
	<div class="section-inner">
		<div class="content fleft">								        
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>	
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="post-header">
						<?php if ( get_the_title() ) : ?>
						    <h1 class="post-title"><?php the_title(); ?></h1>
						<?php endif; ?>
					</div><!-- .post-header -->							                                    	    
					<div class="post-content">
						<?php the_content(); ?>
						<?php wp_link_pages(); ?>
						<div class="clear"></div>			        
					</div><!-- .post-content -->	
					<div class="post-meta-container">
						<div class="post-author">
							<div class="post-author-content">
							<p>written by:</p>
								<h4><?php the_author_meta( 'display_name' ); ?></h4>
								<?php 
								if ( get_the_author_meta( 'description' ) ) { 
									echo wpautop( get_the_author_meta( 'description' ) );
								}
								?>
								<div class="author-links">	
									<?php 
									$author_url = get_the_author_meta( 'user_url' ); 															
									if ( ! empty( $author_url ) ) : ?>
										<a class="author-link-website" href="<?php echo esc_url( $author_url ); ?>" target="_blank"><?php _e( 'find out more', 'baskerville' ); ?></a>
									<?php endif; ?>
								</div><!-- .author-links -->
							</div><!-- .post-author-content -->
						</div><!-- .post-author -->
						<div class="post-meta">
							<p class="post-date"><?php the_time( get_option( 'date_format' ) ); ?></p>
							<?php if ( function_exists( 'zilla_likes' ) ) zilla_likes(); ?>
							<p class="post-categories"><?php the_category( ', ' ); ?></p>
							<?php if ( has_tag() ) : ?>
								<p class="post-tags"><?php the_tags( '', ', ' ); ?></p>
							<?php endif; ?>
							<div class="clear"></div>
							<div class="post-nav">
								<?php
								$prev_post = get_previous_post();
								$next_post = get_next_post();
								if ( $prev_post ) :
									?>
									<a class="post-nav-prev" href="<?php the_permalink( $prev_post->ID ); ?>"><?php _e( 'Previous post', 'baskerville' ); ?></a>
									<?php 
								endif; 
								if ( $next_post ) :
									?>
									<a class="post-nav-next" href="<?php the_permalink( $next_post->ID ); ?>"><?php _e( 'Next post', 'baskerville' ); ?></a>
									<?php 
								endif; 
								edit_post_link( __( 'Edit post', 'baskerville' ) ); 
								?>
								<div class="clear"></div>
							</div><!-- .post-nav -->
						</div><!-- .post-meta -->
						<div class="clear"></div>
					</div><!-- .post-meta-container -->			
					<?php comments_template( '', true ); ?>		                        
				<?php endwhile; endif; ?>
			</div><!-- .post -->
		</div><!-- .content -->
		<?php get_sidebar(); ?>
		<div class="clear"></div>
	</div><!-- .section-inner -->
</div><!-- .wrapper -->
<?php get_footer(); ?>