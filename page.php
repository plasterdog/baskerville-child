<?php get_header(); ?>

<div class="wrapper section medium-padding" id="site-content">
										
	<div class="section-inner">
	
		<div class="content fleft">
	
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
				<div class="post">
				
					<div class="post-header">
												
					    <?php the_title( '<h1 class="post-title">', '</h1>' ); ?>
					    				    
				    </div><!-- .post-header -->
				
		
				   				        			        		                
					<div class="post-content">
								                                        
						<?php 
						the_content();
						wp_link_pages();
						?>
						
						<div class="clear"></div>
															            			                        
					</div><!-- .post-content -->
					
					<?php comments_template( '', true ); ?>
									
				</div><!-- .post -->
			
			<?php endwhile; endif; ?>
		
			<div class="clear"></div>
			
		</div><!-- .content -->
		<div class="sidebar fright" role="complementary">
		     <?php if ( is_active_sidebar( 'page-sidebar' ) ) : ?>
        <?php dynamic_sidebar( 'page-sidebar' ); ?>
		<?php endif; ?>
	</div>
		<div class="clear"></div>
	
	</div><!-- .section-inner -->

</div><!-- .wrapper -->
								
<?php get_footer(); ?>