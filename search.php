<?php get_header(); ?>

<div class="wrapper section medium-padding" id="site-content">										
	<div class="section-inner">
		<div class="content fleft">
		<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'plasterdogflexible' ), '<span>' . get_search_query() . '</span>' ); ?></h1>

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>				
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class ="result-container">
					<div class="post-header">
						<?php if ( get_the_title() ) : ?>
						    <h1 class="post-title"><a href="<?php the_permalink(); ?>" rel="bookmark"> <?php the_title(); ?></a></h1>
						<?php endif; ?>
					</div><!-- .post-header -->		                                    	    
					<div class="post-content">
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="featured-image">											
								<a href="<?php the_permalink(); ?>" rel="bookmark"> <?php the_post_thumbnail( 'thumbnail' ); ?></a>					
						</div><!-- .featured-media -->
						<div class="excerpt-section">
						<?php the_excerpt(); ?>
						</div><!-- ends excerpt ection -->								
					<?php endif; ?>
					<?php if ( !has_post_thumbnail() && ! post_password_required() ) : ?>							                                  
						<?php the_excerpt(); ?>
					<?php endif; ?>	
						<p class="archive-link"><a href="<?php the_permalink(); ?>" rel="bookmark"> Read the full piece</a></p>
						<div class="clear"></div>
					</div><!-- .post-content -->
					<div class="clear"></div>
					</div><!-- .post-meta-container -->													
					</div><!-- ends post container -->
		<?php endwhile; ?>
		<?php else : ?>
<div class ="result-container">
			<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'plasterdogflexible' ); ?></p>
			<?php get_search_form(); ?>

</div>
												                        
				<?php endif; ?>
			</div><!-- .post -->
		</div><!-- .content -->
		<?php get_sidebar(); ?>
		<div class="clear"></div>
	</div><!-- .section-inner -->
</div><!-- .wrapper -->
		
<?php get_footer(); ?>