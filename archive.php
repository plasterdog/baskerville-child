<?php get_header(); ?>

<div class="wrapper section medium-padding" id="site-content">								
	<div class="section-inner">
		<div class="content fleft">
<div class="post">		
<div class="post-content archive-heading">	
<h1 class="page-title"><?php single_cat_title(); ?> </h1>
<?php the_archive_description(  ); ?>
</div>	<!-- ends category head section -->		
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>	
				<div class ="archive-excerpt-container">			
					<div class="post-header">
					 <h1 class="post-title"> <a href="<?php the_permalink(); ?>" rel="bookmark">  <?php the_title( ); ?></a></h1>
				    </div><!-- .post-header -->	        		                
					<div class="post-content">
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="featured-image">
						<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'thumbnail' ); ?></a>
						</div><!-- .featured-media -->
						<div class="excerpt-section">
						<?php the_excerpt(); ?>
						<p class="archive-link"><a href="<?php the_permalink(); ?>" rel="bookmark"> Read the full piece</a></p>
						</div><!-- ends excerpt section -->								
					<?php endif; ?>

					<?php if ( !has_post_thumbnail() && ! post_password_required() ) : ?>            
						<?php the_excerpt(); ?>
						<p class="archive-link"><a href="<?php the_permalink(); ?>" rel="bookmark"> Read the full piece</a></p>
					<?php endif; ?>	
						<div class="clear"></div>									            			                        
					</div><!-- .post-content -->
				</div>
			<?php endwhile; endif; ?>		

		<?php if ( $wp_query->max_num_pages > 1 ) : ?>
		<div class="archive-nav section-inner">
			<div class ="pagination-container">		
			<?php echo get_next_posts_link( '&laquo; ' . __( 'Older posts', 'baskerville' ) ); ?>
			<?php echo get_previous_posts_link( __( 'Newer posts', 'baskerville') . ' &raquo;' ); ?>
			<div class="clear"></div>
			</div>
		</div><!-- .post-nav archive-nav -->				
			<?php endif; ?>						
		</div><!-- .post -->
			<div class="clear"></div>
		</div><!-- .content -->

<!-- MARKUP IS FOUND IN PARENT'S sidebar.php FILE -->
		<?php get_sidebar(); ?>
		<div class="clear"></div>
	</div><!-- .section-inner -->
</div><!-- .wrapper -->
<?php get_footer(); ?>