<?php
// setting up the child

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
 
    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.
 
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
/**
 * Register widgetized area and update sidebar with default widgets.
 */
function plasterdog_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Page Sidebar', 'plasterdog' ),
		'id'            => 'page-sidebar',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>'
	) );


}
add_action( 'widgets_init', 'plasterdog_widgets_init' );


//* -JMC-Replace WordPress login logo with your own
add_action('login_head', 'b3m_custom_login_logo');
function b3m_custom_login_logo() {
echo '<style type="text/css">
h1 a { background-image:url('.get_stylesheet_directory_uri().'/images/login.png) !important; background-size: 209px 150px !important;height: 150px !important; width: 209px !important; margin-bottom: 0 !important; padding-bottom: 0 !important; }
.login form { margin-top: 10px !important; }
</style>';
}
// JMC- custom footer message
function modify_footer_admin () {
  echo 'Site design by <a href="http://www.plasterdog.com/">Plasterdog Web Design</a>. ';
  echo 'CMS Powered by<a href="http://WordPress.org"> WordPress </a>!';
}
add_filter('admin_footer_text', 'modify_footer_admin');
// JMC Remove WordPress Widgets from Dashboard Area
function remove_wp_dashboard_widgets(){

    remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');  // Incoming Links
    remove_meta_box('dashboard_quick_press', 'dashboard', 'side');  // Quick Press
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');  // Recent Drafts
    remove_meta_box('dashboard_primary', 'dashboard', 'side');   // WordPress blog (News)
    
}
add_action('wp_dashboard_setup', 'remove_wp_dashboard_widgets');

//Remove  WordPress Welcome Panel
remove_action('welcome_panel', 'wp_welcome_panel');
// JMC - change the standard wordpress greeting
add_action( 'admin_bar_menu', 'wp_admin_bar_my_custom_account_menu', 11 );

function wp_admin_bar_my_custom_account_menu( $wp_admin_bar ) {
$user_id = get_current_user_id();
$current_user = wp_get_current_user();
$profile_url = get_edit_profile_url( $user_id );

if ( 0 != $user_id ) {
/* Add the "My Account" menu */
$avatar = get_avatar( $user_id, 28 );
$howdy = sprintf( __('Restricted Access Area (logged in editor): %1$s'), $current_user->display_name );
$class = empty( $avatar ) ? '' : 'with-avatar';

$wp_admin_bar->add_menu( array(
'id' => 'my-account',
'parent' => 'top-secondary',
'title' => $howdy . $avatar,
'href' => $profile_url,
'meta' => array(
'class' => $class,
),
) );

}
}


/* JMC- remove html filters from category descriptions*/
$filters = array( 'pre_term_description' );

foreach ( $filters as $filter ) {
    remove_filter( $filter, 'wp_filter_kses' );
}

foreach ( array( 'term_description' ) as $filter ) {
    remove_filter( $filter, 'wp_kses_data' );
}
// JMC - allows shortcodes in widgets
add_filter('widget_text', 'do_shortcode');
/**--- JMC OBSCURES LOGIN FAILURE MESSAGE---*/
     add_filter('login_errors',create_function('$a', "return null;"));

//JMC remove anchor link from more tag

function remove_more_anchor($link) {
     $offset = strpos($link, '#more-');
     if ($offset) {
          $end = strpos($link, '"',$offset);
     }
     if ($end) {
          $link = substr_replace($link, '', $offset, $end-$offset);
     }
     return $link;
}
add_filter('the_content_more_link', 'remove_more_anchor');

// JMC - COMBINING BOTH CUSTOM WIDGETS INTO A SINGLE WIDGET
add_action('wp_dashboard_setup', 'my_dashboard_widgets');

function my_dashboard_widgets() {
     global $wp_meta_boxes;
     unset(
          $wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins'],
          $wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary'],
          $wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']
     );

add_meta_box( 'dashboard_custom_feed', 'Welcome to your customized site!', 'dashboard_custom_feed_output', 'dashboard', 'side', 'high' );
}
function dashboard_custom_feed_output() {
     echo '<div class="rss-widget">';
     echo '<p>Your site has been significantly customized and many functions exist only in your theme, so think twice before changing it!</p>
 <p>Have a question? contact Jeff McNear by email: <a href="mailto:jeff@plasterdog.com">here</a>. </p>
<p>Old school? give me a call at: 847/849-7060</p>
<p>For a list of tutorials <a href="http://plasterdog.com/category/wordpress-tutorials/" target="_blank">Follow this link</a>
<p><strong>Here are some recent tutorials:</strong></p><hr/>';
     wp_widget_rss_output(array(
          'url' => 'http://plasterdog.com/category/wordpress-tutorials/feed/',
          'title' => 'MY_FEED_TITLE',
          'items' => 5,
          'show_summary' => 0,
          'show_author' => 0,
          'show_date' => 0
     ));

     echo '</div>';
}

     
